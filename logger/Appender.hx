
package hxtils.logger;

import hxtils.logger.Utils;

import haxe.io.Output;
import haxe.io.Path;
import sys.io.File;
import sys.FileSystem;


#if neko
  import neko.vm.Thread;
  import neko.vm.Lock;
  import neko.Lib;
#elseif cpp
  import cpp.vm.Thread;
  import cpp.vm.Lock;
  import cpp.Lib;
#end


class StdErrAppender implements IAppender {

  var o:Output;
  static var inst:StdErrAppender;

  function new() {
    this.o = Sys.stderr();
    inst = this;
  }

  static public function getInstance() {
    return inst == null ? new StdErrAppender() : inst;
  }

  public function append( level:Level, m:String ) {
    this.o.writeString( m  + "\n");
  }

}

class StdOutAppender implements IAppender {

  private var o:Output;
  static var inst:StdOutAppender;

  function new() {
    this.o = Sys.stdout();
    inst = this;
  }

  static public function getInstance() {
    return inst == null ? new StdOutAppender() : inst;
  }

  public function append( level:Level, m:String ) {
    this.o.writeString( m   + "\n" );
  }
}

class FileAppender implements IAppender {

  var o:Output;

  var filename:String;      // the name user specified for log file make contain date formating
  var current_file:String;  // generated filename

  public function new( file:String ) {
    filename = file;
    check_filename();
  }

  public function append( level:Level, m:String ) {
    check_filename();
    this.o.writeString( m + "\n");
    // FIXME: This might not be a good idea
    // but for the sake of tailing the log file
    this.o.flush();
  }

  function check_filename() {
    var fname = DateTools.format(Date.now(), filename);
    if( current_file !=  fname ) {
      check_dir(fname);
      current_file = fname;
      this.o = File.append( fname, false );
    }
  }

  function check_dir(path) {
    var p = new Path(path);
    if( !FileSystem.exists(p.dir) ) {
      check_dir(p.dir);
      FileSystem.createDirectory(p.dir);
    }
  }

}

class ThreadAppender implements IAppender {

    static private var t:Thread;
    static private var tarray:Array<IAppender>;
    static private var tarray_lock:Lock;

    static private function tlogger() {
      do {
        var a = Thread.readMessage(true);
        for( ap in tarray )
          ap.append( a.level, a.msg );
      } while( true );

    }

    public function new() {
      if( t == null ) {
        tarray_lock = new Lock();
        tarray = new Array();
        t = Thread.create(tlogger);
      }
    }

    public function append( level:Level, m:String ) {
      t.sendMessage({"level":level, "msg":m});
    }


    public function addSubAppender(ia:IAppender) {
      tarray.push(ia);
    }
}