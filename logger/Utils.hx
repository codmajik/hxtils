
package hxtils.logger;


enum Level {
  EMERGENCY;
  ALERT;
  CRITICAL;
  ERROR;
  WARNING;
  NOTICE;
  INFORMATION;
  DEBUG;
}


class LoggerTools {
    static public function LevelToInt(lvl:Level):Int {
      return switch(lvl) {
              case Level.EMERGENCY: 0;
              case Level.ALERT: 1;
              case Level.CRITICAL: 2;
              case Level.ERROR: 3;
              case Level.WARNING: 4;
              case Level.NOTICE: 5;
              case Level.INFORMATION: 6;
              case Level.DEBUG: 7;
            }
    }
}


interface IAppender {
  public function append( level:Level, m:String ):Void;
}

interface ILogger {
  public var log_format:String;
  public var timestamp_format:String;

  public function info( msg:String, ?arg:Dynamic  ):Void;
  public function warn( msg:String, ?arg:Dynamic  ):Void;
  public function error( msg:String, ?arg:Dynamic  ):Void;
  public function alert( msg:String, ?arg:Dynamic  ):Void;
  public function debug( msg:String, ?arg:Dynamic  ):Void;
  public function notice( msg:String, ?arg:Dynamic  ):Void;
  public function critical( msg:String, ?arg:Dynamic  ):Void;
  public function emergency( msg:String, ?arg:Dynamic  ):Void;

  public function log( level:Level, msg:String, ?arg:Dynamic,  ?pos:haxe.PosInfos  ):Void;
}