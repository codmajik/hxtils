
package hxtils.logger;

import neko.Lib;
import hxtils.logger.Utils;

private static var LIBNAME:String = "ndll/" + neko.Sys.systemName() + "/xtils"


public enum LogToStdErr {
  OFF,
  ON_LOG_ERROR,
  ON
}


public class SyslogAppender implements IAppender {
  private static var inited:Bool = False;

  public function new(ident:String, toStdErr:LogToStdErr = LOGToStdErr.OFF, IncludePID:Bool = True) {
    if( !inited ) {
      var to_std_err = switch(toStdErr) {
          case LogToStdErr.OFF: return 0;
          case LogToStdErr.ON_LOG_ERROR: return 1;
          case LogToStdErr.ON: return 2;
        }

      inited = _openlog(ident, to_std_err, IncludePID );
    }
  }


  public function append(level:Level, msg:String):Void {
    if( inited == false ) return;
    _syslog( LoggerTools.LevelToInt(level), msg );
  }

  public function close():Void {
    _closelog();
    inited = false;
  }

  private static var _openlog = Lib.load(LIBNAME, "nsyslog_openlog", 3);
  private static var _syslog = Lib.load(LIBNAME, "nsyslog_syslog", 2);
  private static var _closelog = Lib.load(LIBNAME, "nsyslog_closelog", 0);
}