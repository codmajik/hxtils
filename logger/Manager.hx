
package hxtils.logger;

import hxtils.logger.Appender;
import hxtils.logger.Utils;

import haxe.Stack;
#if neko
import neko.Lib;
#elseif cpp
import cpp.Lib;
#end


private typedef LogRecord = {
  name: String,
  level: Level,
  ?module: String,
  ts:Date,
  ?lineno: Int,
  msg: String,
  ?exc_info: Dynamic,
  ?func: String,
  ?filename: String
}


private class LogBox {
  // Toolbox -> Used across logging classes
  static public var l:Hash<Logger>;
  static public var appenders:haxe.FastList<IAppender>;

  static function __get_field(r:Dynamic, k:String) {
    return Reflect.isObject(r) ? Reflect.field(r, k) : null;
  }

  static function log__repl(rec:LogRecord, args:Dynamic, ts_fmt:String, r:EReg):String {

    var key = r.matched(1);
    switch( key ) {
      case "exc_info":
      case "is_exception":
        return "%[" + key + "]%";
    }

    var a = __get_field(rec,key);
    if( a == null ) a = __get_field(args,key);

    if( a != null && Std.is(a, Date) ) {
      var dt:Date = cast(a);
      a = DateTools.format( dt, ts_fmt );
    }

    return a == null ? "%[" + key + "]%" : a;
  }

  static public function format( fmt:String, time_fmt:String, rec:LogRecord, arg:Dynamic ) {
    var rx:EReg = ~/%\[([a-zA-Z0-9-_]+)\]%/im;
    return rx.customReplace(fmt, callback(LogBox.log__repl, rec, arg, time_fmt));
  }
}


private class Logger implements ILogger {

  private var name:String;

  public var log_format:String;
  public var timestamp_format:String;

  public function new(name:String) {

    this.name = name;

    // set default format
    this.log_format = LogManager.TRACE_LOG_FMT;
    this.timestamp_format = "%F %T";
  }

  public function debug( msg:String, ?arg:Dynamic ) {
    this._log(Level.DEBUG, msg, arg);
  }

  public function info( msg:String, ?arg:Dynamic  ) {
    this._log(Level.INFORMATION, msg, arg);
  }

  public function notice( msg:String, ?arg:Dynamic  ) {
    this._log(Level.NOTICE, msg, arg);
  }

  public function warn( msg:String, ?arg:Dynamic  ) {
    this._log(Level.WARNING, msg, arg);
  }

  public function error( msg:String, ?arg:Dynamic  ) {
    this._log(Level.ERROR, msg, arg);
  }

  public function critical( msg:String, ?arg:Dynamic  ) {
    this._log(Level.CRITICAL, msg, arg);
  }

  public function alert( msg:String, ?arg:Dynamic  ) {
    this._log(Level.ALERT, msg, arg);
  }

  public function emergency( msg:String, ?arg:Dynamic  ) {
    this._log(Level.EMERGENCY, msg, arg);
  }

  public function log(level:Level, msg:String, ?arg:Dynamic, ?p:haxe.PosInfos ) {
    this._log(level, msg, arg, p);
  }


  // PRIVATE STUFF
  private function _log( l_:Level, m_:String, arg:Dynamic, ?pos:haxe.PosInfos ) {

    var rec:LogRecord = { name: this.name, ts: Date.now(), level: l_, msg: m_ };

    if( pos != null ) {
      rec.filename = pos.fileName;
      rec.lineno = pos.lineNumber;
      rec.func = pos.methodName;

      if( pos.className.length > 0 )
        rec.func = pos.className + "::" + pos.methodName;

    } else switch( Stack.callStack()[2] ) {
        case Module(m): rec.module = m;
        case Method(cl, m): rec.func = cl + "::" + m;
        case Lambda(p): rec.lineno = p;
        case FilePos(s, file, lin):
            rec.filename = file;
            rec.lineno = lin;
        case CFunction:
            rec.func = "<[Native CFunction]>";
    }

    rec.msg = LogBox.format( rec.msg, this.timestamp_format, rec, arg );
    var log_msg = LogBox.format( this.log_format, this.timestamp_format, rec, null );

    if( LogBox.appenders == null ) {
      Lib.println("No Appends -> Logging Message going into BlackHole");
      LogManager.removeAppender( null );
    }

    for( ap in LogBox.appenders )
      ap.append( rec.level, log_msg );
  }
}


class LogManager {

    static public var STD_LOG_FMT(default,never):String = "%[ts]% %[level]%: %[msg]%";
    static public var STD_LOG_FMT_NAME(default,never):String = "%[ts]% %[name]% %[level]%: %[msg]%";
    static public var TRACE_LOG_FMT(default,never):String = "%[filename]%:%[lineno]% " + STD_LOG_FMT;
    static private var DEFAULT_FMT:String = STD_LOG_FMT;
    static private var traceLevel:Level = Level.DEBUG;
    static private var traceLogger:ILogger;

    static public function getLogger( name:String, ?fmt:String):ILogger {

      var lg = LogBox.l;
      if( lg == null )
        lg = LogBox.l = new Hash<Logger>();

      var l = lg.get( name );
      if( l == null ) {
        l = new Logger(name);
        l.log_format = fmt == null ?  DEFAULT_FMT : fmt;
        lg.set( name, l );
      }

      return l;
    }

    static public function setDefaultFormat( fmt:String ) {
      DEFAULT_FMT = fmt;
    }

    static public function addAppender( appender:IAppender ) {
      removeAppender(appender);
      LogBox.appenders.add(appender);
    }

    static public function removeAppender( appender:IAppender ):Bool {

      var ap = LogBox.appenders;
      if(ap == null)
        ap = LogBox.appenders = new haxe.FastList<IAppender>();

      return ap.remove(appender);
    }


    static private function logTrace(str:Dynamic, ?p:haxe.PosInfos) {
      traceLogger.log(traceLevel, Std.string(str), null, p);
    }

    static public function enableTraceLogging(l:Level):Void {
      if( traceLogger == null ) {
        traceLogger = new Logger("TRACER");
        traceLogger.log_format = DEFAULT_FMT;
      }

      haxe.Log.trace = logTrace;
      traceLevel = l;
    }
}

typedef ILog = ILogger;