
#include "includes/hxutils_base.hxx"
#include "syslog.h"


value nsyslog_openlog(value ident, value stderr, value with_pid) {

  auto opt = LOG_ODELAY;

  THROW_EXCEPTION( val_is_string(ident), "Expected String for Ident" );

  if( write_to_stderr != val_null ) {

    THROW_EXCEPTION(val_is_int(stderr), "Invalid Write to StdErr Value");

    switch( val_int(stderr) ) {
        case 1: opt |= LOG_CONS;    // write to stderr on syslog fail
        case 0: break;
        default: opt |= LOG_PERROR; // Always write to stderr
    }
  }

  THROW_EXCEPTION( val_is_bool(with_pid), "Invalid Show PID in Log value" );
  if( val_bool(with_pid) ) opt |= LOG_PID;

  openlog(val_string(ident), opt, LOG_USER);
  return val_true;
}


value nsyslog_closelog() {
  closelog();
  return val_true;
}

value nsyslog_syslog(value level, value msg) {

  auto loglevel = LOG_INFO;

  static auto _map = {
      LOG_EMERG,
      LOG_ALERT,
      LOG_CRIT,
      LOG_ERR,
      LOG_WARNING,
      LOG_NOTICE,
      LOG_INFO,
      LOG_DEBUG
  };

  THROW_EXCEPTION(val_is_int(level), "level expected Int");

  int lvl = val_int(level);
  if( lvl >= 0  &&  lvl < 8)
    loglevel = _map[lvl];

  syslog(loglevel, "%s", val_string(msg));
  return val_true;
}


DEFINE_PRIM(nsyslog_openlog, 3);
DEFINE_PRIM(nsyslog_closelog, 0);
DEFINE_PRIM(nsyslog_syslog, 2);