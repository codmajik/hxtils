
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "neko_vm.h"

#define THROW_EXCEPTION(cond, msg) \
  static char m[1024]; auto n=sprintf(&m,"%s(): %s" __FUNCTION__, msg); \
  if (!(cond) ) { val_throw(copy_string(&m, n)); \
  return val_null; }